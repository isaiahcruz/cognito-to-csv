const fs = require('fs');

const csv = (data, path) => new Promise((resolve, reject) => {
  fs.writeFile(path, data, 'utf8', function (err) {
    if (err) {
      reject('An error occurred.');
    } else {
      resolve();
    }
  });
});

module.exports = csv;
