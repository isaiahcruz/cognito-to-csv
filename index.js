const AWSCognitoIdentityServiceProvider = require('aws-sdk/clients/cognitoidentityserviceprovider');
const csv = require('./csv');

require('dotenv/config');

const cognito = new AWSCognitoIdentityServiceProvider({
  region: process.env.REGION,
});

const defaults = {
  'cognito:mfa_enabled': 'false',
  phone_number_verified: 'false',
};

(async () => {
  const { Users } = await cognito.listUsers({
    UserPoolId: process.env.USER_POOL_ID,
  }).promise();


  const { CSVHeader: headers } = await cognito.getCSVHeader({
    UserPoolId: process.env.USER_POOL_ID,
  }).promise();

  const data = Users.map((user) => {
    const row = headers.map((header) => {
      if (header in defaults) {
        return defaults[header];
      }

      const attribute = user.Attributes
        .find(({ Name }) => Name === (
          header === 'cognito:username' ? 'email' : header
        ));

      if (attribute) {
        return attribute.Value;
      }

      return '';
    })

    return row;
  });

  await csv([headers, ...data].join('\n'), 'users.csv');
})();
